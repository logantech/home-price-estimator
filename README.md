# Home Price Estimator

* Created a Python 3 notebook to analyze home sales data and create a machine learning model to predict pricing.
* Used Tableau to create an [exploratory visualization](https://public.tableau.com/profile/logan.downing#!/vizhome/KingCountyHomeSales/Dashboard) of all 21,600 datapoints.

Quickstart:

* Download Python 3 notebook: LoganDowning-M04-HomePriceEstimator.ipynb
* Download datasource: kc_house_data.csv
* Open using Jupyter Notebook

Or view these [screen grabs](screenshots/) of the entire notebook.
___

Analyzed King County home sales data with multiple features including house size, location, view and lot sizes.

![Visualization of the distribution of home prices by region](images/region-distribution.png)


Used Tableau to build [visualization](https://public.tableau.com/profile/logan.downing#!/vizhome/KingCountyHomeSales/Dashboard) of the 21,600 homes sales in King County from 2015, including filters to help explore patterns and Google satellite views.

![Visualization of home prices in King County](images/viz.png)

Examined correlations of features to determine the strongest and weakest influences on price.

![Correlation plot of all features](images/corr.png)

 Hypothesis testing of multiple features to help explain differences in price.

![Results of hypothesis testing](images/hypothesis-testing.png)

Created pricing model using linear least squares regression on engineered set of features, using stepwise feature reduction, SVD analysis, and principal component analysis.

![Output of pricing model](images/model-output.png)
